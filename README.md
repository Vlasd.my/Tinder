## Change Log

Mini tutorials : [Git](docs/tutorials/GIT.md), 
[Maven](docs/tutorials/MAVEN.md), 
[Linux Aliases](docs/tutorials/LINUX_ALIASES.md), [AWS EC2](docs/tutorials/AWS_EC2.md).

#### Next Tasks

1. Create account on [aws.amazon.com](https://aws.amazon.com/). We need to make our application from internet.
2. Create EC2 instance following [tutorial](docs/tutorials/AWS_EC2.md).
3. Go through [JPA HOMESTUDY](docs/homestudy/JPA.md).



#### 20.1.2018 Online Store : JPA

1. Deployed project to AWS.
2. Implemented JPA approach to work with database tables
3. Implemented DBUtils with static connection holder.


#### 18.1.2018 Online Store : Auth

1. Added login, logout page.
2. Saved Cookies for loged in users.
3. Added login check in Http Filter.

![](docs/img/login-page.png)


#### 16.1.2018 CRM Reports

Dmytro Vlasenko: Implemented `getClientById` function.

In the classroom:  

1. Implemented simple "like" queries with in JDBC.
2. Made inner, right SQL joins.


#### 13.1.2018 Online Store : JDBC

1. Familiarise yourself with SQL, JDBC, Mysql [HOMESTUDY](docs/homestudy/JDBC_SQL.md).
2. Connect our Store with Mysql.

#### 12.1.2018

Dmytro Vlasenko :  
1. Add server endpoint that serve all Products in JSON format utilizing Jackson library.
2. Add server endpoint that serve Products from Cart in JSON format utilizing Jackson library.

#### 11.1.2018 Online Store : Servlets

1. Implemented Hello World with Servlets.
2. Handling POST method that, add product to the cart.

![](docs/img/online_store.png)
