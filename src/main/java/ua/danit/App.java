package ua.danit;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(loadOnStartup = 1, urlPatterns = "/")
public class App extends HttpServlet {

  UserDao userDao = new UserDao();
  public static int i = 0;

  public  List<User> allUsers = userDao.getListUsers();




  private final static String START = "<html>" +
      "<head>" +
      "<style>" +
      ".id_user {" +
      "display: none;" +
      "}" +
      "</style>" +
      "</head>" +
      "<body>";

  private final static String MAIN = "<img src = \"%1$s\" alt=\"# \">" +
      "<form action = \"/messenges\" method = \"POST\">" +
      "<input name = \"name\" type = \"text\" value = \"%2$s\">" +
      "<input class =\"id_user\" name = \"id\" type = \"text\" value = \"%3$d\">" +
      "<button name =\"btn\" type = \"submit\" value = \"Yes\">Yes</button>" +
      "<button name =\"btn\" type = \"submit\" value = \"No\">No</button>" +
      "</form>";

  private final static String END = "<a href = \"/messenges\">Messenges</a></body></html>";

  public App() throws SQLException {
  }

  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    response.getWriter().write(buildPage());
  }

  private String buildPage() {
    if (i < allUsers.size()) {
      StringBuilder result = new StringBuilder();
      result.append(START);
      result.append(String.format(MAIN, allUsers.get(i).getPhoto(), allUsers.get(i).getName(), allUsers.get(i).getId()));
      result.append(END);
      return result.toString();
    } else {
      return "End of list";
    }

  }
}