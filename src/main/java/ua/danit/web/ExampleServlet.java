package ua.danit.web;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(loadOnStartup = 1, urlPatterns = "/example")
public class ExampleServlet extends HttpServlet {

  private static final String PAGE_START = "<!DOCTYPE html>\n" +
      "<html lang=\"en\">\n" +
      "<head>\n" +
      "    <meta charset=\"UTF-8\">\n" +
      "    <title>Title</title>\n" +
      "</head>\n" +
      "<body>\n" +
      "    <div>\n" +
      "        \n" +
      "        <ul>";

  private static final String ELEMENT = "<li><img src=\"%1$s\" alt=\"#\">%2$s</li>";

  private static final String PAGE_END = "</ul>\n" +
      "    </div>\n" +
      "</body>\n" +
      "</html>";

  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
    response.getWriter().write(buildPage());
  }

  private String buildPage() {
    StringBuilder res = new StringBuilder();
    res.append(PAGE_START);

    return res.toString();
  }
}
