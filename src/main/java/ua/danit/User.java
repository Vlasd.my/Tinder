package ua.danit;

public class User {

  private long id;
  private String photo;
  private String name;
  private String password;
  private String email;

  public User(long id, String photo, String name, String password, String email) {
    this.id = id;
    this.photo = photo;
    this.name = name;
    this.password = password;
    this.email = email;
  }

  public long getId() {
    return id;
  }

  public String getPhoto() {
    return photo;
  }

  public String getName() {
    return name;
  }

  public String getPassword() {
    return password;
  }

  public String getEmail() {
    return email;
  }
}
