package ua.danit;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "login", loadOnStartup = -1, urlPatterns = "/login")
public class LoginServlet extends HttpServlet {
  UserDao userDao = new UserDao();

  public  Map<String, String> knownUsers = userDao.getLoginPassword();

  public LoginServlet() throws SQLException {
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    PrintWriter out = response.getWriter();
    out.write("<html><body>");
    out.write("  <form action='/login' method='POST'>");
    out.write("    <label>User Name</label>");
    out.write("    <input name='userName' type='text'><br>");

    out.write("    <label>Password</label>");
    out.write("    <input name='password' type='password'><br>");

    out.write("    <button type='submit'>Login</button>");
    out.write("  </form>");
    out.write("</body></html>");
  }

  public static Map<String, String> tokens = new HashMap<String, String>();

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    String userName = request.getParameter("userName");
    String password = request.getParameter("password");

    boolean known = knownUsers.containsKey(userName)
        && knownUsers.get(userName).equals(password);

    if (known) {
      String token = UUID.randomUUID().toString();
      tokens.put(token, userName);
      response.addCookie(new Cookie("user-token", token));
      response.sendRedirect("/");
    } else {
      response.getWriter().write(String.format("I dont know you, %s", userName));
    }
  }

}

