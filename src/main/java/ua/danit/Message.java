package ua.danit;

import java.util.ArrayList;

public class Message {
    private final boolean isMessageFromFirstSpeaker;
    private final String text;

    public Message(boolean isMessageFromFirstSpeaker, String text) {
        this.isMessageFromFirstSpeaker = isMessageFromFirstSpeaker;
        this.text = text;
    }

    public boolean isMessageFromFirstSpeaker() {
        return isMessageFromFirstSpeaker;
    }

    public String getText() {
        return text;
    }

}
