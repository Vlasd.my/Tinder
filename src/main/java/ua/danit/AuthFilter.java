package ua.danit;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebFilter(filterName = "cookieFilter", urlPatterns = "/*")
public class AuthFilter implements Filter {

  static Set<String> publicUrls = new HashSet<String>() {
    {
      add("/login");
    }
  };

  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
    HttpServletRequest req = (HttpServletRequest) request;
    HttpServletResponse resp = (HttpServletResponse) response;
    Cookie[] cookies = req.getCookies();
    boolean loggedIn = false;

    for (Cookie cookie : cookies) {
      String name = cookie.getName();
      String value = cookie.getValue();
      if (name.equals("user-token")  && LoginServlet.tokens.containsKey(value)) {
        loggedIn = true;
        break;
      }
    }

    if (loggedIn || publicUrls.contains(req.getRequestURI())) {
      chain.doFilter(request, response);
    } else {
      resp.sendRedirect("/login");
    }
  }
  @Override
  public void init(FilterConfig filterConfig) throws ServletException { }
  @Override
  public void destroy() { }
}



