package ua.danit;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(loadOnStartup = 1, urlPatterns = "/messenges")
public class MessServ extends HttpServlet {

  UserDao userDao = new UserDao();

  public List<Long> chosenUsersId = new ArrayList<>();
  public List<User> chosenUsers;

  private final static String START = "<html><head><style>\n" +
      "\n" +
      "        li {\n" +
      "            list-style-type: none;\n" +
      "            border: 1px solid black;\n" +
      "            width: 400px;\n" +
      "            height: 40px;\n" +
      "            margin-bottom: 10px;\n" +
      "            display: flex;\n" +
      "            align-items: center;\n" +
      "            padding: 8px;\n" +
      "            box-sizing: bo;\n" +
      "        }\n" +
      "    </style></head><body><ul>";

  private final static String MAIN = "<li>%1$s</li>";

  private final static String END = "</ul></body></html>";

  public MessServ() throws SQLException {
  }

  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    String answer = request.getParameter("btn");
    if (answer.equals("Yes")) {
//      String id = request.getParameter("id");
//      chosenUsersId.add(Long.parseLong(id));
    }
    App.i++;
    response.sendRedirect("/");
  }

  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
//    try {
//      chosenUsers = userDao.getChosenUsersById(chosenUsersId);
//    } catch (SQLException e) {
//      e.printStackTrace();
//    }

    response.getWriter().write(buildPage());
  }

  private String buildPage() {
    StringBuilder result = new StringBuilder(START);
    for (User user : chosenUsers) {
      result.append(String.format(MAIN, user.getName()));
    }
    result.append(END);
    return result.toString();
  }
}
