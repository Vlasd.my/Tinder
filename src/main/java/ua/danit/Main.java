package ua.danit;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;

public class Main {
  public static void main(String[] args) throws IOException {
    BufferedInputStream in = new BufferedInputStream(new FileInputStream("/tmp/File.txt"));
    byte[] buffer = new byte[1024];

    while (in.available() > 0) {
      int length = in.read(buffer);
      System.out.write(buffer, 0, length);
    }
  }
}
