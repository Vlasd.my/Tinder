package ua.danit;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserDao {
  public UserDao() {
  }

  String DB_URL = "jdbc:mysql://danit.cukm9c6zpjo8.us-west-2.rds.amazonaws.com:3306/fs2";
  String USER =  "fs2_user";
  String PASSWD = "new_york";

  public ArrayList<User> getListUsers() throws SQLException {


    ArrayList<User> users = new ArrayList<>();

    Connection con = DriverManager.getConnection(DB_URL, USER, PASSWD);
    String sql = "SELECT ID, EMAIL, NAME, PASSWORD, PHOTO FROM USERS_T";
    Statement statement = con.createStatement();
    ResultSet resultSet = statement.executeQuery(sql);

    while (resultSet.next()) {

      Long id = resultSet.getLong("ID");
      String name = resultSet.getString("NAME");
      String email = resultSet.getString("EMAIL");
      String password = resultSet.getString("PASSWORD");
      String photo = resultSet.getString("PHOTO");

      users.add(new User(id, photo, name, password, email));
    }

    resultSet.close();
    statement.close();
    con.close();

    return users;
  }

  public HashMap<String, String> getLoginPassword() throws SQLException {
    HashMap<String, String> loginPassword = new HashMap<String, String>();

    ArrayList<User> users = getListUsers();

    for (User user : users) {
      loginPassword.put(user.getName(), user.getPassword());
    }


    return loginPassword;
  }

  public ArrayList<User> getChosenUsersById(List<Long> chosenId) throws SQLException {


    ArrayList<User> users = new ArrayList<>();

    for (Long el : chosenId) {
      Connection con = DriverManager.getConnection(DB_URL, USER, PASSWD);
      String sql = "SELECT ID, EMAIL, NAME, PASSWORD, PHOTO FROM USERS_T WHERE ID = %d";
      Statement statement = con.createStatement();
      ResultSet resultSet = statement.executeQuery(String.format(sql,el));

      while (resultSet.next()) {
        Long id = resultSet.getLong("ID");
        String name = resultSet.getString("NAME");
        String email = resultSet.getString("EMAIL");
        String password = resultSet.getString("PASSWORD");
        String photo = resultSet.getString("PHOTO");

        users.add(new User(id, email, name, password, photo));
      }
      resultSet.close();
      statement.close();
      con.close();
    }



    return users;
  }

  public ArrayList<String> getNameLikedUsers(Long id) {
    ArrayList<String> nameLikedUsers = new ArrayList<>();
    return nameLikedUsers;
  }
}