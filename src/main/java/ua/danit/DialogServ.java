package ua.danit;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(loadOnStartup = 1, urlPatterns = "/dialog/*")
public class DialogServ extends HttpServlet {

    public static List<Message> messages = new ArrayList<>();

    static {
        messages.add(new Message(true, "Hi! How are you?"));
        messages.add(new Message(false, "Hello. I`m fine"));
        messages.add(new Message(true, "What are you doing today?"));
        messages.add(new Message(true, "Well I will be all day at DAN.IT"));
        messages.add(new Message(false, "I will stay at home"));
    }

    private final static String PAGE_START = "<html lang=\"en\">\n" +
            "<head>\n" +
            "    <meta charset=\"UTF-8\">\n" +
            "    <title>Title</title>\n" +
            "</head>\n" +
            "<body>\n" +
            "<div class=\"container\">\n" +
            "    <ul>";
    private final static String PAGE_MESSAGE_FROM_FIRST_SPEAKER = "<li class=\"first_speaker\">%s</li>";

    private final static String PAGE_MESSAGE_FROM_SECOND_SPEAKER = "<li class=\"second_speaker\">%s</li>";

    private final static String MESSAGE_FORM = " </ul>\n" +
            "    <form class=\"send\" action=\"/dialog/*\" method=\"post\">\n" +
            "        <input name = \"text\" type=\"text\">\n" +
            "        <button type=\"submit\">Send</button>\n" +
            "    </form>";

    private final static String PAGE_END = "</div>\n" +
            "</body>\n" +
            "</html>";

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.getWriter().write(buildPage());
    }

    private String buildPage() {
        StringBuilder page = new StringBuilder();
        page.append(PAGE_START);
        for (Message m: messages) {
            if (m.isMessageFromFirstSpeaker()) {
                page.append(String.format(PAGE_MESSAGE_FROM_FIRST_SPEAKER, m.getText()));
            } else {
                page.append(String.format(PAGE_MESSAGE_FROM_SECOND_SPEAKER, m.getText()));
            }
        }
        page.append(MESSAGE_FORM);
        page.append(PAGE_END);
        return page.toString();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String text = request.getParameter("text");
        messages.add(new Message(true, text));
        response.sendRedirect("/dialog/");
    }


}
